#include "main.h"

static int csteam_init(lua_State *L){
	
	bool success = SteamAPI_Init();
	lua_pushboolean(L, success);
	return 1;
}

static int csteam_getusername(lua_State *L){
	const char *name = SteamFriends()->GetPersonaName();
	lua_pushstring(L, name);
	return 1;
}

extern "C" {
	__declspec(dllexport) int luaopen_csteam(lua_State *L) {
		lua_newtable(L);
		luax_tfunction(L, "init",     csteam_init);
		luax_tfunction(L, "username", csteam_getusername);

		return 1;
	}
}
