#include "common.h"
#include <string>


void luax_printstack(lua_State * L) {
	for (int i = 1; i <= lua_gettop(L); i++)
		std::cout << "Stack: " << i << " - " << luaL_typename(L, i) << std::endl;
}

const char * uint64ToChar(uint64 num) {
	return std::to_string(num).c_str();
}

const char * intToChar(uint64 num) { return to_string(num).c_str(); }

const char * intToChar(uint32 num) { return to_string(num).c_str(); }

const char * intToChar( int32 num) { return to_string(num).c_str(); }

string uint64ToString(uint64 num)  { return to_string(num); }

uint64 stringToUint64(string str) {
	std::string::size_type sz = 0;
	return std::stoull(str, &sz, 0);
}

uint32 stringToUint32(string str) {
	std::string::size_type sz = 0;
	return std::stoul(str, &sz, 0);
}

void luax_pushint_32(lua_State * L, uint32 value) {
	lua_pushstring(L, intToChar(value));
}

uint32 luax_checkint_32(lua_State * L, int idx) {
	const char *str = luaL_checkstring(L, idx);
	return stringToUint32(std::string(str));
}

void luax_pushint_64(lua_State * L, uint64 value) {
	lua_pushstring(L, intToChar(value));
}

uint64 luax_checkint_64(lua_State * L, int idx) {
	const char *str = luaL_checkstring(L, idx);
	return stringToUint64(std::string(str));
}



void luax_tint_32(lua_State * L, char * key, uint32 value) {
	lua_pushstring(L, key); lua_pushstring(L, intToChar(value)); lua_rawset(L, -3);
}

void luax_tint_32(lua_State * L, int key,    uint32 value) {
	lua_pushnumber(L, key); lua_pushstring(L, intToChar(value)); lua_rawset(L, -3);
}


void luax_tint_64(lua_State * L, char * key, uint64 value) {
	lua_pushstring(L, key); lua_pushstring(L, intToChar(value)); lua_rawset(L, -3);
}

void luax_tint_64(lua_State * L, int key, uint64 value) {
	lua_pushnumber(L, key); lua_pushstring(L, intToChar(value)); lua_rawset(L, -3);
}

void luax_tnumber(lua_State * L, char * key, float value) {
	lua_pushstring(L, key); lua_pushnumber(L, value); lua_rawset(L, -3);
}
void luax_tnumber(lua_State * L, int key, float value) {
	lua_pushnumber(L, key); lua_pushnumber(L, value); lua_rawset(L, -3);
}

string uint_32_to_ip(uint32 ip) {
	uint8 ip_array[4] = { 0, 0, 0, 0 };
	char * ip_addr = new char[16];
	for (int i = 0; i < 4; i++)
		ip_array[i] = ip >> (i * 8);
	sprintf(ip_addr, "%u.%u.%u.%u", ip_array[3], ip_array[2], ip_array[1], ip_array[0]);
	return string(ip_addr);
}

uint32 ip_to_uint_32(string ip) {
	char ip_array[4];
	sscanf(ip.c_str(), "%uhh.%uhh.%uhh.%uhh", &ip_array[3], &ip_array[2], &ip_array[1], &ip_array[0]);
	return ip_array[0] | ip_array[1] << 8 | ip_array[2] << 16 | ip_array[3] << 24;
}

void luax_tboolean(lua_State * L, char * key, bool value) {
	lua_pushstring(L, key); lua_pushboolean(L, value); lua_rawset(L, -3);
}

void luax_tboolean(lua_State * L, string * key, bool value) {
	luax_pushString(L, *key); lua_pushboolean(L, value); lua_rawset(L, -3);
}

void luax_tboolean(lua_State * L, int key, bool value) {
	lua_pushnumber(L, key); lua_pushboolean(L, value); lua_rawset(L, -3);
}


void luax_tfunction(lua_State * L, char * key, lua_CFunction value) {
	lua_pushstring(L, key); lua_pushcfunction(L, value); lua_rawset(L, -3);
}

void luax_tfunction(lua_State * L, int key, lua_CFunction value) {
	lua_pushnumber(L, key); lua_pushcfunction(L, value); lua_rawset(L, -3);
}


void luax_tstring(lua_State * L, char * key, const char * value) {
	lua_pushstring(L, key); lua_pushstring(L, value); lua_rawset(L, -3);
}

void luax_tstring(lua_State * L, int key, const char * value) {
	lua_pushnumber(L, key); lua_pushstring(L, value); lua_rawset(L, -3);
}

std::string luax_toString(lua_State *L, int idx)
{
	size_t len;
	const char *str = lua_tolstring(L, idx, &len);
	return std::string(str, len);
}

std::string luax_checkString(lua_State *L, int idx)
{
	size_t len;
	const char *str = luaL_checklstring(L, idx, &len);
	return std::string(str, len);
}

void luax_pushString(lua_State *L, const std::string &str) {
	lua_pushlstring(L, str.data(), str.size());
}

void luax_tString(lua_State *L, char * key, const std::string &str) {
	lua_pushstring(L, key);  lua_pushlstring(L, str.data(), str.size()); lua_rawset(L, -3);
}

void luax_tString(lua_State *L, int key, const std::string &str) {
	lua_pushnumber(L, key);  lua_pushlstring(L, str.data(), str.size()); lua_rawset(L, -3);
}


const char * luax_tgetstring(lua_State * L, int key) {
	lua_pushnumber(L, key);
	lua_gettable(L, -2);
	const char * value = luaL_checkstring(L, -1);
	lua_pop(L, 1);
	return value;
}

const char * luax_tgetstring(lua_State * L, const char * key) {
	lua_pushstring(L, key);
	lua_gettable(L, -2);
	const char * value = luaL_checkstring(L, -1);
	lua_pop(L, 1);
	return value;
}

int luax_tgetnumber(lua_State * L, int key) {
	lua_pushnumber(L, key);
	lua_gettable(L, -2);
	int value = luaL_checknumber(L, -1);
	lua_pop(L, 1);
	return value;
}

int luax_tgetnumber(lua_State * L, const char * key) {
	lua_pushstring(L, key);
	lua_gettable(L, -2);
	int value = luaL_checknumber(L, -1);
	lua_pop(L, 1);
	return value;
}

bool luax_tgetboolean(lua_State * L, int key) {
	lua_pushnumber(L, key);
	lua_gettable(L, -2);
	bool value = lua_toboolean(L, -1);
	lua_pop(L, 1);
	return value;
}

bool luax_tgetboolean(lua_State * L, const char * key) {
	lua_pushstring(L, key);
	lua_gettable(L, -2);
	bool value = lua_toboolean(L, -1);
	lua_pop(L, 1);
	return value;
}


int luax_error(lua_State *L, const char * value) {
	lua_pushstring(L, value); lua_error(L); return 0;
}

int luax_error2(lua_State *L, const char * value) {
	lua_pushnil(L), lua_pushstring(L, value); return 2;
}