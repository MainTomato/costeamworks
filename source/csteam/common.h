#pragma once
#ifndef COMMON_H
#define COMMON_H
	extern "C" {
		#include "lua.h"
		#include "lauxlib.h"
	}

	#include <vector>
	#include <string>
	#include <iostream>
	#include <stdarg.h>
	#include "steam_api.h"
	#include "isteamuser.h"
	#include "isteamfriends.h"
	#include "memory"

	using namespace std;

	// serealize uint64 to char *
	const char * uint64ToChar(uint64 num);

	// serealize number to char *
	const char * intToChar(uint64 num);

	const char * intToChar(uint32 num);

	const char * intToChar(int32  num);

	// serealize uint64 to std::string
	string uint64ToString(uint64 num);

	// serealize uint32 to std::string
	string uint_32_to_ip(uint32 ip);

	// deserealize uint32 from std::string
	uint32 ip_to_uint_32(string ip);

	// deserealize int64 to std::string
	uint64 stringToUint64(std::string str);

	// print current content of lua-stack to std out
	void luax_printstack(lua_State * L);

	// return uint32 on top of stack
	uint32 luax_checkint_32(lua_State * L, int idx);

	// push int32 to top of stack
	void luax_pushint_32(lua_State * L, uint32 value);

	// return uint64 on top of stack
	uint64 luax_checkint_64(lua_State * L, int idx);

	// push int64 to top of stack
	void luax_pushint_64(lua_State * L, uint64 value);

	// push int_32 transformed to string to table on top of stack with key/value as char *, int_32
	void luax_tint_32(lua_State * L, char * key, uint32 value);
	// push int_64 transformed to string to table on top of stack with key/value as int, int_64
	void luax_tint_32(lua_State * L, int    key, uint32 value);

	// push int_64 transformed to string to table on top of stack with key/value as char *, int_64
	void luax_tint_64(lua_State * L, char * key, uint64 value);
	// push int_64 transformed to string to table on top of stack with key/value as int, int_64
	void luax_tint_64(lua_State * L, int    key, uint64 value);

	// push string to table on top of stack with key/value as char *, char *
	void luax_tstring(lua_State * L, char * key, const char * value);
	// push string to table on top of stack with key/value as int, char *
	void luax_tstring(lua_State * L, int    key, const char * value);

	// push float to table on top of stack with key/value as char *, int
	void luax_tnumber(lua_State * L, char * key, float value);
	// push float to table on top of stack with key/value as int, int
	void luax_tnumber(lua_State * L, int    key, float value);

	// push boolean to table on top of stack with key/value as string *, bool
	void luax_tboolean(lua_State * L, string * key, bool value);
	// push boolean to table on top of stack with key/value as char *, bool
	void luax_tboolean(lua_State * L, char * key, bool value);
	// push boolean to table on top of stack with key/value as int, bool
	void luax_tboolean(lua_State * L, int    key, bool value);

	// push function to table on top of stack with key/value as char *, cfunction
	void luax_tfunction(lua_State * L, char * key, lua_CFunction value);
	// push function to table on top of stack with key/value as int, cfunction
	void luax_tfunction(lua_State * L, int    key, lua_CFunction value);

	// return std::string on top of stack
	std::string luax_toString(lua_State * L,    int idx);
	// return std::string on top of stack
	std::string luax_checkString(lua_State * L, int idx);
	// push std::string to top of stack
	void luax_pushString(lua_State * L, const std::string & str);
	// push std::string to table on top of stack with key/value as char *, std::string
	void luax_tString(lua_State * L, char * key, const std::string & str);
	// push std::string to table on top of stack with key/value as int, std::string
	void luax_tString(lua_State * L, int    key, const std::string & str);

	// get const char * from table on top of stack with key as int
	const char * luax_tgetstring(lua_State * L, int key);

	// get const char * from table on top of stack with key as const char *
	const char * luax_tgetstring(lua_State * L, const char * key);

	// get int from table on top of stack with key as int
	int luax_tgetnumber(lua_State * L, int key);
	// get int from table on top of stack with key as const char *
	int luax_tgetnumber(lua_State * L, const char * key);
	// get boolean from table on top of stack with key as int
	bool luax_tgetboolean(lua_State * L, int key);
	// get boolean from table on top of stack with key as const char *
	bool luax_tgetboolean(lua_State * L, const char * key);

	// push const char * and call lua_error
	int luax_error(lua_State *L, const char * value);
	// push nil and const char *, return 2 as arg count
	int luax_error2(lua_State *L, const char * value);


#endif // COMMON_H
